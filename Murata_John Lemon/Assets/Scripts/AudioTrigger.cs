﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour
{
    public AudioClip[] audioClipWood;

    public AudioClip[] audioClipTiles;

    public AudioSource audioSource;
    public int floorType = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tiles")
        {
            floorType = 1;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Tiles")
        {
            floorType = 0;
        }
    }

    public void triggerAudio()
    {
        audioSource.pitch = (Random.Range(0.8f, 1.1f));
        switch (floorType)
        {
            case 0:
                audioSource.PlayOneShot(audioClipWood[Random.Range(0, audioClipWood.Length)], 1f);
                break;
            case 1:
                audioSource.PlayOneShot(audioClipTiles[Random.Range(0, audioClipTiles.Length)], 0.7f);
                break;
        }
    }
}