﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioController : MonoBehaviour
{
    void Start()
    {
        GetComponentInParent<AudioSource>().pitch = Random.Range(0.8f, 1.1f);
        GetComponentInParent<AudioSource>().time = Random.Range(0f, GetComponentInParent<AudioSource>().clip.length);
    }
}