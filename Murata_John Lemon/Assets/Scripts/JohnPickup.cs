﻿
using System.Threading;
using UnityEngine;
using TMPro;

public class JohnPickup : MonoBehaviour
{
    /// <summary>
    /// The number of lemons.
    /// </summary>
    int m_Lemons;

    

    /// <summary>
    /// The collectible layer.
    /// </summary>
    int m_CollectibleLayer;

    private int count;
    public TextMeshProUGUI countText;

    private void Start()
    {
        // Initialise the number of lemons to 0
        m_Lemons = 0;

        SetCountText();

        count = 0;

        // Initialise the layer using the name we gave it
        m_CollectibleLayer = LayerMask.NameToLayer("Collectible");
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
    }

    // This is called when John enters ANY Trigger collider
    private void OnTriggerEnter(Collider other)
    {

        // If John touched an object in the Collectible layer
        if (other.gameObject.CompareTag("PickUp"))
        {
            count = count + 1;

            SetCountText();

           
            // Get the LemonPickupable component on the game object we touched
            var pickupable = other.gameObject.GetComponent<LemonPickupable>();
            if (pickupable != null)
            {
                // Call the PickupObject method
                pickupable.PickupObject();
                // Increase the number of lemons John has
                m_Lemons++;
            }
        }
    }
}